# Asterisk Configuration

These configuration files act as a skeleton for a simple Asterisk PBX deployment using a SIP trunk to provide one DID, with two internal extensions. The majority of the configuration files are documented inline and are self explanatory.

This configuration has been tested on the following Linux distributions:
* Ubuntu Server 18.04 | Asterisk 13.18.3

The following SIP providers were used to construct these configuration files:
* [Twilio](https://www.twilio.com/sip-trunking)

## Installation

1. Install the latest version of Asterisk from your distribution's repositories.

```bash
$ sudo apt install asterisk -y
```

2. Remove the distribution's configuration files from `/etc/asterisk/`.

```bash
$ sudo rm -rf /etc/asterisk/*
```

3. Download these configuration files to `/etc/asterisk/`.

```bash
$ ls /etc/asterisk/
extensions.conf  modules.conf  sip.conf  voicemail.conf
```

4. Modify the configuration to suit your needs. Refer to the configuration hints for more details.

## Configuration hints

### Configuration files

* `sip.conf`

This file's primary purpose is to define how Asterisk, trunks and extensions communicate and authenticate with one another.

Two templates are provided, each containing the common properties required to create either a new trunk or extension.

* `extensions.conf`

This configuration file details how calls should move through Asterisk.

Contexts are used to namespace internal extensions, incoming calls and outgoing calls. This prevents abuse and makes future modifications easier to integrate.

* `modules.conf`

This file is required to load Asterisk's modules. Asterisk is able to automatically load modules, so this file shouldn't need touching.

* `voicemail.conf`

The voicemail configuration file details how voicemails work.

### Configuration modifications

#### Create a new trunk

In `sip.conf`, create a new trunk which inherits from the `trunk` template. 

Provide at the very least, a `context` and a `host`. Optionally, provide a `fromuser`.

* `context`: Incoming calls will be redirected to this context.
* `fromuser`: This is the caller ID which will appear when placing outbound calls (N.B. Twilio requires this parameter).
* `host`: This defines how Asterisk connects to the trunk.

If authentication is used by your SIP provider, the credentials should also be inserted into this trunk definition.
```
[newtrunk](trunk)
    context=441234567890
    fromuser=441234567890
    host=example.pstn.ie1.twilio.com
```

Remembering that incoming calls from the trunk will be redirected to its `context`, create a new context in `extensions.conf` of the same name.
```
[441234567890]
    exten => _+X.,1,Goto(internal,0,1)
```
This `exten` line states: "For all incoming calls whose caller ID starts with a '+' and is of any length, dial extension `0` in the `internal` context."

To use this trunk for outbound calls, add a new context in `extensions.conf` and reference it within the `internal` context's dialplan.
```
[outgoing_newtrunk]
    exten => _X.,1,Dial(SIP/newtrunk/+${EXTEN})
    same => n,Hangup
```
This `exten` line states: "For all outgoing calls whose caller ID is of any length, prepend the caller ID with a '+' and then call it using the `newtrunk` SIP trunk." 

```
[internal]
    ...
    exten => _+X,1,Goto(outgoing_newtrunk,${EXTEN:1},1)
```
See the extisting outgoing redirects for more examples of how to parse dialed numbers.

#### Create a new extension
In `sip.conf`, creat e a new extension that inherits from the `internal` template.

This only requires a `secret` to be specified.
```
[1234](internal)
    secret=supersecurepassword
```
Create a new dialplan in the `internal` context in `extensions.conf`.
```
[internal]
    ...
    exten => 1000,1,Dial(SIP/1000,10)
    same => n,VoiceMail(${EXTEN})
    same => n,Hangup
```
Configure the voicemail for this extension in `voicemail.conf`.